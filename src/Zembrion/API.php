<?php
namespace Zembrion;

use \Curl\Curl;
/**
 * Provides access to the endpoints of the API of Zembrion.
 * See URL for details.
 */
class API {
    protected $version = 'v1';

    protected $api_key;
    protected $url;
    protected $user_agent;
    public $requests;

    /**
     * Initialize the class to perform requests to a specific store.
     *
     * @param int $store_id The id of the store
     * @param string $access_token The access token obtained from the Auth class
     * @param string $user_agent The user agent to use to identify your app
     */
    public function __construct($apiKey, $user_agent){
        $this->api_key = $apiKey;
        $this->user_agent = $user_agent;
        $this->requests = new Requests;        
        $this->url = "http://zembrion.com/api/{$this->version}/";        
    }
    
    /**
     * Make a GET request to the specified path.
     *
     * @param string $path The path to the desired resource
     * @param array $params Optional parameters to send in the query string
     * @return Zembrion/API/Response
     */
    public function get($path, $params = null){
        $url_params = '';
        if (is_array($params)){
            $url_params = '?' . http_build_query($params);
        }
        
        

        return $this->_call('GET', $path . $url_params);
    }


    public function createMedia($mediaFile,$mediaOptions){
        $curl = new Curl();
        set_time_limit(0);

        $curl->setUserAgent($this->user_agent);
        $curl->setReferrer('');
        $curl->setHeader('X-Authorization-Zembrion', $this->api_key);
        $curl->setHeader('Content-Type', 'multipart/form-data');
    
        //Seting File
        $d = new \CurlFile($mediaFile);
        $mediaFile = realpath($mediaFile);        
        $fileInfo =   pathinfo($mediaFile)  ;        
        $mediaOptions["filename"] = $fileInfo['filename'];
        $mediaOptions["extension"] = $fileInfo['extension'];
        $mediaOptions["file_contents"] = $d;

        $filesize = filesize($mediaFile);         

        $curl->setOpt(CURLOPT_INFILESIZE, filesize($mediaFile));
        $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 0);
        $curl->setOpt(CURLOPT_TIMEOUT, 120 * 1000);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);

        $response = $curl->post($this->url . "upload", $mediaOptions);            



        if ($curl->error) {            
            throw new API\Exception($curl);            
        }else {
            if (!is_object($response)){
                $curl->error = 1;
                $curl->error_code = 100101;
                $curl->error_message = "Zembrion Error (".$response.")";
                throw new API\Exception($curl);       
            }
            $response = new API\Response($this, $curl);
            return $curl->response;
        }        
    }



    public function createMediaXX($mediaFile,$mediaOptions){
        $mediaFile = realpath($mediaFile);        

        $fileInfo =   pathinfo($mediaFile)  ;

        $d = new \CurlFile($mediaFile);
        

        $headers = array('X-Authorization-Zembrion: '.$this->api_key ,'Content-Type: multipart/form-data');         
        $options = array('timeout' => 10,'useragent' => $this->user_agent);
        $filesize = filesize($mediaFile);         
         
        $ch = curl_init();
        
        $mediaOptions["filename"] = $fileInfo['filename'];
        $mediaOptions["extension"] = $fileInfo['extension'];

        $mediaOptions["file_contents"] = $d;

        $options = array(
            CURLOPT_URL => $this->url . "upload",
            CURLOPT_HEADER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $mediaOptions,
            CURLOPT_INFILESIZE => $filesize,
            CURLOPT_RETURNTRANSFER => true
        ); 

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);

        if(!curl_errno($ch)){
            $info = curl_getinfo($ch);            
            if ($info['http_code'] == 200){}
        }else{
            $errmsg = curl_error($ch);
        }
        
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);                 
        $header = substr($response, 0, $header_size);
        $body = json_decode(substr($response, $header_size));        

        return $body;
    }
    
    /**
     * Make a POST request to the specified path.
     *
     * @param string $path The path to the desired resource
     * @param array $params Parameters to send in the POST data
     * @return Zembrion/API/Response
     */
    public function post($path, $params = []){
        $json = json_encode($params);
        
        return $this->_call('POST', $path, $json);
    }
    
    /**
     * Make a PUT request to the specified path.
     *
     * @param string $path The path to the desired resource
     * @param array $params Parameters to send in the PUT data
     * @return Zembrion/API/Response
     */
    public function put($path, $params = []){
        $json = json_encode($params);
        
        return $this->_call('PUT', $path, $json);
    }
    
    /**
     * Make a DELETE request to the specified path.
     *
     * @param string $path The path to the desired resource
     * @return Zembrion/API/Response
     */
    public function delete($path){
        return $this->_call('DELETE', $path);
    }





    protected function _callPost($path, $data = null){

    }






    protected function _call($method, $path, $data = null){
        $headers = [
            'X-Authorization-Zembrion' => "{$this->api_key}",
            'Content-Type' => 'application/json',
        ];
        
        $options = [
            'timeout' => 10,
            'useragent' => $this->user_agent,
        ];
        
        
        $response = $this->requests->request($this->url . $path, $headers, $data, $method, $options);
        $response = new API\Response($this, $response);
        if ($response->status_code == 404){
            throw new API\NotFoundException($response);
        } elseif (!in_array($response->status_code, [200, 201])){
            throw new API\Exception($response);
        }
        
        return $response;
    }
}
