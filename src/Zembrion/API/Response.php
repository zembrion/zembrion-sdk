<?php
namespace Zembrion\API;
use \Curl\Curl;
/**
 * Contains the headers and body of the response to an API request.
 */
class Response {
    public $body;
    public $headers;
    public $status_code;
    public $main_language;
    
    private $api;

    public function __construct($api, $response){
        if ($response Instanceof Curl){
            
            $this->status_code = 200;
            $this->headers = $response->request_headers;
            $this->body = $response->response;
        }else{
            $this->status_code = $response->status_code;
            $this->body = json_decode($response->body);
            $this->headers = $response->headers;
        }
        //$this->main_language = isset($response->headers['X-Main-Language']) ? $response->headers['X-Main-Language'] : null;
        
        $this->api = $api;
    }    
}
